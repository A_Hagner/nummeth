#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

double functions(int i, double x);

void backsub(gsl_matrix*A, gsl_vector*c);

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R);

void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x);

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B);

int print_matrix(gsl_matrix * M);

int print_vector(gsl_vector * M);

void lsfit_alloc(gsl_vector*x,gsl_vector*y,gsl_vector*dy, int n, double f(int i,double x),gsl_vector*c);



int main(int argc, char **argv) {
    size_t n=10;

    gsl_matrix*Data=gsl_matrix_alloc(n,3);
    //gsl_matrix*S=gsl_matrix_alloc(3,3);
    gsl_vector*c=gsl_vector_alloc(3);

    FILE* handle=fopen("Data.txt","r");

    gsl_matrix_fscanf(handle,Data);

    gsl_vector*x=gsl_vector_alloc(n);
    gsl_vector*y=gsl_vector_alloc(n);
    gsl_vector*dy=gsl_vector_alloc(n);

    gsl_matrix_get_col(x,Data,0);
    gsl_matrix_get_col(y,Data,1);
    gsl_matrix_get_col(dy,Data,2);

    gsl_matrix_free(Data);

    lsfit_alloc(x,y,dy,3,functions,c);

    for(double z=0.05;z<=5;z+=0.05){
        fprintf(stdout,"%g \t %g \n",z,gsl_vector_get(c,0)*1/z+gsl_vector_get(c,1)+gsl_vector_get(c,2)*z);
    }


    //gsl_matrix_free(S);
    gsl_vector_free(c);
return 0;}


































