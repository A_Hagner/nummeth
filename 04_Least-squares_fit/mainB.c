#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>


void print_matrix(gsl_matrix * M);

void print_vector(gsl_vector * M);

void lsfit_unc_alloc(gsl_vector*x,gsl_vector*y,gsl_vector*dy, int n, double f(int i,double x),gsl_vector*c, gsl_matrix*covariance);

double functions(int i, double x);





int main(int argc, char **argv) {
    size_t n=10;

    gsl_matrix*Data=gsl_matrix_alloc(n,3);
    gsl_matrix*S=gsl_matrix_alloc(3,3);
    gsl_vector*c=gsl_vector_alloc(3);

    FILE* handle=fopen("Data.txt","r");

    gsl_matrix_fscanf(handle,Data);

    gsl_vector*x=gsl_vector_alloc(n);
    gsl_vector*y=gsl_vector_alloc(n);
    gsl_vector*dy=gsl_vector_alloc(n);

    gsl_matrix_get_col(x,Data,0);
    gsl_matrix_get_col(y,Data,1);
    gsl_matrix_get_col(dy,Data,2);

    gsl_matrix_free(Data);

    lsfit_unc_alloc(x,y,dy,3,functions,c,S);


/*
    for(double z=0.05;z<=5;z+=0.05){
        fprintf(stdout,"%g \t %g \n",z,gsl_vector_get(c,0)*1/z+gsl_vector_get(c,1)+gsl_vector_get(c,2)*z);
    }
*/
    size_t m=3;
    gsl_vector* dc = gsl_vector_alloc(3);

    for(int k=0;k<m;k++){
        double skk=gsl_matrix_get(S,k,k);
        gsl_vector_set(dc,k,sqrt(skk));
    }

    double fit(double x){
        double s=0;
        for(int k=0;k<m;k++)s+=gsl_vector_get(c,k)*functions(k,x);
        return s;
    }

    double fit_plus(double x){
        double s=0;
        for(int k=0;k<m;k++)s+=(gsl_vector_get(c,k)+gsl_vector_get(dc,k))*functions(k,x);
        return s;
    }

    double fit_minus(double x){
        double s=0;
        for(int k=0;k<m;k++)s+=(gsl_vector_get(c,k)-gsl_vector_get(dc,k))*functions(k,x);
        return s;
    }


    double dz=( gsl_vector_get(x,n-1) - gsl_vector_get(x,0) )/90.0;
    double z=gsl_vector_get(x,0)- dz/2.0;

    fprintf(stdout,"x\tf\tfp\tfm\n");
    do{
        fprintf(stdout,"%g\t%g\t%g\t%g\n",z,fit(z),fit_plus(z),fit_minus(z));
        z+=dz;
    }while(z<gsl_vector_get(x,n-1)+dz);














    gsl_matrix_free(S);

    gsl_vector_free(c);
    gsl_vector_free(dc);
    gsl_vector_free(x);
    gsl_vector_free(y);
    gsl_vector_free(dy);

    return 0;
}
