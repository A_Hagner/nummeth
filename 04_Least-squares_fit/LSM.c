#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

double functions(int i, double x){
    if(i==0){return 1.0/x;}
    else if(i==1){return 1.0;}
    else if(i==2){return x;}
    else{fprintf(stderr,"Wrong i (chose 0<=i<=3"); return NAN;}
}

void backsub(gsl_matrix*A, gsl_vector*c){
    for(int i=c->size-1; i>=0; i--){
        double ci=gsl_vector_get(c,i);
        for(int j=i+1; j<c->size; j++){
            ci -= gsl_matrix_get(A, i, j) * gsl_vector_get(c, j);
        }
        gsl_vector_set(c,i,ci/gsl_matrix_get(A,i,i));
    }
}

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R){
    double Rij;

    for(int i=0;i<A->size2;i++){
        gsl_vector_view ai=gsl_matrix_column(A,i); //Lets you work with vectors while they are in a matrix
        double Rii=(gsl_blas_dnrm2(&ai.vector)); //calculating diagonal elements
        gsl_matrix_set(R,i,i,Rii); //setting Rii diagonal
        gsl_vector_scale(&ai.vector,1.0/Rii);

        for(int j=i+1;j<A->size2;j++){
            gsl_vector_view aj=gsl_matrix_column(A,j);

            gsl_blas_ddot (&ai.vector,&aj.vector,&Rij); //DOTS
            gsl_matrix_set(R,i,j,Rij);
            gsl_blas_daxpy (-Rij,&ai.vector , &aj.vector); //This does a*x+y

            gsl_vector_scale(&ai.vector,1.0/gsl_blas_dnrm2(&ai.vector));
        }

    }

}

void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x){

    gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); //Dles Q^T*b saves in x. (Found in BLAS level 2)

    backsub(R,x);
}

//returns inverse in B
void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B){
    gsl_matrix_set_identity(B);
    gsl_vector*x = gsl_vector_alloc(Q->size1);

    for(int i=0;i<Q->size1;i++){
        gsl_vector_view b=gsl_matrix_column(B,i);
        qr_gs_solve(Q,R,&b.vector,x);
        gsl_matrix_set_col(B,i,x);
    }
}

void print_matrix(gsl_matrix * M){
    int n=(*M).size1, m=(*M).size2;
    for (int row=0; row<n; row++)
    {
        for(int columns=0; columns<m; columns++){
            printf("%10.3f     ", gsl_matrix_get(M,row,columns));}
        printf("\n");
    }
}

void print_vector(gsl_vector * M){
    int n=(*M).size;
    for (int row=0; row<n; row++){
        printf("%10.3f", gsl_vector_get(M,row));
    printf("\n");}
}



void lsfit_alloc(gsl_vector*x,gsl_vector*y,gsl_vector*dy, int n, double f(int i,double x),gsl_vector*c){
    int m=x->size;
    gsl_matrix*A=gsl_matrix_alloc(m,n);
    gsl_matrix*R=gsl_matrix_alloc(n,n);
    gsl_matrix*Ainv=gsl_matrix_alloc(n,n);
    gsl_vector*b=gsl_vector_alloc(m);


    gsl_vector_memcpy(b,y);

    gsl_vector_div(b,dy);

    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            gsl_matrix_set(A,i,j,f(j,gsl_vector_get(x,i))/gsl_vector_get(dy,i));
        }
    }

    qr_gs_decomp(A,R);

    qr_gs_solve(A,R,b,c);

    gsl_matrix_free(A);
    gsl_matrix_free(Ainv);
    gsl_matrix_free(R);
    gsl_vector_free(b);
}


void lsfit_unc_alloc(gsl_vector*x,gsl_vector*y,gsl_vector*dy, int n, double f(int i,double x),gsl_vector*c, gsl_matrix*covariance){
    int m=x->size;
    gsl_matrix*A=gsl_matrix_alloc(m,n);
    gsl_matrix*R=gsl_matrix_alloc(n,n);
    gsl_matrix*Ainv=gsl_matrix_alloc(n,n);
    gsl_vector*b=gsl_vector_alloc(m);
    gsl_matrix *I    = gsl_matrix_alloc(n,n);
    gsl_matrix* inverse_R = gsl_matrix_alloc(n,n);

    gsl_vector_memcpy(b,y);

    gsl_vector_div(b,dy);

    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            gsl_matrix_set(A,i,j,f(j,gsl_vector_get(x,i))/gsl_vector_get(dy,i));
        }
    }

    qr_gs_decomp(A,R);

    qr_gs_solve(A,R,b,c);



    gsl_matrix_set_identity(I);

    qr_gs_inverse(I,R,inverse_R);


    gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,inverse_R,inverse_R,0.0,covariance);


    gsl_matrix_free(A);
    gsl_matrix_free(Ainv);
    gsl_matrix_free(R);
    gsl_matrix_free(inverse_R);


    gsl_vector_free(b);
}



