#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>
#include <assert.h>

double adapt24(double f(double x), double a, double b, double eps, double acc, double f2, double f3, int n ){
    assert(n<1000000);
    double f1 = f(a+(b-a)/6.0);
    double f4 = f(a+5.0/6.0 *(b-a) );

    double Q = (b-a)*( 2.0/6.0*f1 + f2/6.0 + f3/6.0 + 2.0/6.0*f4 );
    double q = (b-a)*(f1+f2+f3+f4)/4.0;
    double dQ = fabs(Q-q);

    double tolerance = acc + eps*fabs(Q);
    if(dQ < tolerance){return Q;}
    else{
        double Q1 = adapt24(f, a, (b+a)/2, eps, acc/sqrt(2.0), f1, f2, n+1 );
        double Q2 = adapt24(f, (b+a)/2, b, eps, acc/sqrt(2.0), f1, f2, n+1 );
        return Q1+Q2;
    }
}




double adapter(double f(double x), double a, double b, double eps, double acc){
    int n=0;
    double f2 = f(a+2.0*(b-a)/6.0);
    double f3 = f(a+4.0*(b-a)/6.0);

    return adapt24(f, a, b, eps, acc, f2, f3, n);
}

/*
//Clenshaw-Curtis for infinite intervals use
double clenshaw_curtis(double f(double x), double acc, double eps){
    double g(double t){
        return f( 1/(tan(t)*sin(t)) )*(1+cos(t)*cos(t))*pow(sin(t),-3);
    }
    return adapter(g,0,M_PI,acc,eps);
}
*/




double adaptw_inf(double f(double x), double a, double b, double eps, double acc ){


    if( isinf(a) && isinf(b) ) {
        double g(double t){return f( t/(1-t*t) )*(1+t*t)*pow(1-t*t,-2);}
        return adapter(g,-1,1,eps,acc);
    }
    else if( isinf(a)==0 && isinf(b) ) {
        double g(double t){return f( a + t/( 1-t ) )/pow(1-t,2);}
        return adapter(g, 0.0, 1.0, eps, acc);
    }
    else if( isinf(a)  && isinf(b)==0 ) {
        double g(double t){return f( b + t/(1+t) )/pow(1+t,2);}
        return adapter(g, -1.0, 0.0, eps, acc);
    }
    else{ return  adapter(f,a,b,eps,acc); }
    //fprintf(stderr,"WHAT?");
}































