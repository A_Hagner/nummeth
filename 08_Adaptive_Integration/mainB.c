#include<stdio.h>
#include<math.h>


double adapter(double f(double x), double a, double b, double eps, double acc);

double adaptw_inf(double f(double x), double a, double b, double eps, double acc);

int main(){

    fprintf(stdout,"Exercise B) \n Adaptive integration with Infinite Limits \n");
    fprintf(stdout,"Example: Integration of exp(-x²) from 0 to inf.\n");

	double fexp(double t){return exp(-t*t);}

    double result = adaptw_inf(fexp, 0, INFINITY, 1e-5, 1e-5);
    fprintf(stdout,"int(0,inf) exp(-x²)dx = %g\n",result);
	fprintf(stdout,"Exact: int(0,inf) exp(-x²)dx = sqrt(pi)/2 = 0.886227\n");
}
