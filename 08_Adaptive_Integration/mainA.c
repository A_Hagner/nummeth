#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

double adapter(double f(double x), double a, double b, double eps, double acc);


int main(){
    int calls=0;

    /*
    double fqrst(double x){
        calls++;
        return sqrt(x);
    }
    */

    double fexerciseA(double x){
        calls++;
        return 4*sqrt( 1 - (1-x)*(1-x) );
    }

    double acc = 1e-10W;
    double eps = 1e-10;
    double a=0.0;
    double b=1.0;

    double QexerciseA = adapter(fexerciseA, a, b, eps, acc);
    fprintf(stdout,"Exercise A) \n Recursive Adaptive Integration\n");
    fprintf(stdout,"Integral from 0 to 1 of 4*sqrt(1-(1-x)²)\n");
    fprintf(stdout,"Adaptive integrator gives, int(0,1) f(x) dx = %g\n", QexerciseA);
    fprintf(stdout,"In #%i calls \n", calls);

    return 0;
}
