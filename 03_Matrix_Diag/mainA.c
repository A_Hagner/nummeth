#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>


void print_matrix(gsl_matrix*M);

void print_vector(gsl_vector*M);

int jacobi_all(gsl_matrix*A,gsl_vector*e,gsl_matrix*V);

int main() {

    srand(time(NULL));

    size_t n = 4;
    gsl_matrix *A = gsl_matrix_alloc(n, n);
    gsl_matrix *AA = gsl_matrix_alloc(n, n);
    gsl_matrix *V = gsl_matrix_alloc(n, n);
    gsl_matrix *D= gsl_matrix_alloc(n, n);
    gsl_matrix *VTA = gsl_matrix_alloc(n, n);
    gsl_vector *e = gsl_vector_alloc(n);



    for(int i=0;i<n;i++){
        for(int j=0;j<n;j++){
            double x = 5 * ((double) rand() / (double) RAND_MAX);
            gsl_matrix_set(A,i,i,x);
        }
    }
    for(int i=0; i<n;i++){
        for(int j=i+1;j<n;j++ ){
            double x = 5 * ((double) rand() / (double) RAND_MAX);
            gsl_matrix_set(A,i,j,x);
            gsl_matrix_set(A,j,i,x);
        }
    }

    gsl_matrix_memcpy (AA,A);
    printf("Matrix A\n");
    print_matrix(A);

    int sweeps=jacobi_all(A,e,V);

    printf("Matrix Eigen Vector\n");
    print_matrix(V);
    printf("Vector eigenvalues\n");
    print_vector(e);

    gsl_blas_dgemm (CblasTrans,CblasNoTrans, 1.0,V,AA,0.0,VTA);
    gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,VTA,V,0.0,D);

    //Why doesnt this work?
    for(int i=0;i<n;i++){
        if(gsl_vector_get(e,i)==gsl_matrix_get(D,i,i)){fprintf(stderr,"VTAV=D: TRUE");}
    }

    printf("Matrix V^TAV\n");
    print_matrix(D);

}











