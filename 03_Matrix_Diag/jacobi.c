#include<math.h>
#include<stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <assert.h>

int jacobi_all(gsl_matrix*A,gsl_vector*e,gsl_matrix*V){
    int n=A->size1;
    for(int i=0;i<n;i++) {
        gsl_vector_set(e, i, gsl_matrix_get(A, i, i)); //Pick Diagonal elements of A
    }
        gsl_matrix_set_identity(V);
        int sweep=0,changed,p,q;

    //Here we do the jacobi diagonalization A'=J^T*A*J
    //We do it clasically in a row
    do {
        changed = 0;
        sweep++;
        for (p = 0; p < n; p++){
            for (q = p+1; q < n; q++) {
                //THe pp,qq,pq'ed elements in A (we starte in a01 a00 (remains the same))
                double app = gsl_vector_get(e, p); //Used for phi in tan for rotation
                double aqq = gsl_vector_get(e, q); //tan(2*phi)=2*apq/(aqq-app)
                double apq = gsl_matrix_get(A, p, q);

                double phi = atan2(2.0*apq, aqq - app)/2.0; //find phi for rotation matrix

                double c = cos(phi);
                double s = sin(phi);

                double app1 = c * c * app - 2.0 * s * c * apq + s * s * aqq; //App'
                double aqq1 = s * s * app + 2.0 * s * c * apq + c * c * aqq; //Aqq'

                if (app1 != app || aqq1 != aqq) {
                    changed = 1; //So loop wont end, we do so it ends when it doesnt change after a sweep because then changed=0
                    gsl_vector_set(e, p, app1);
                    gsl_vector_set(e, q, aqq1);
                    gsl_matrix_set(A, p, q, 0.0);


                    //moving on to Api',Aqi',Aip',Aiq'
                    for (int i = 0; i < p; i++) {
                        double aip = gsl_matrix_get(A, i, p);
                        double aiq = gsl_matrix_get(A, i, q);
                        gsl_matrix_set(A, i, p, aip * c - s * aiq);
                        gsl_matrix_set(A, i, q, aiq * c + s * aip);
                    }

                    for (int i = p + 1; i < q; i++) {
                        double api = gsl_matrix_get(A, p, i);
                        double aiq = gsl_matrix_get(A, i, q);
                        gsl_matrix_set(A, p, i, api * c - s * aiq);
                        gsl_matrix_set(A, i, q, aiq * c + s * api);
                    }

                    for (int i = q + 1; i < n; i++) {
                        double api = gsl_matrix_get(A, p, i);
                        double aqi = gsl_matrix_get(A, q, i);
                        gsl_matrix_set(A, p, i, api * c - s * aqi);
                        gsl_matrix_set(A, q, i, aqi * c + s * api);
                    }

                    for (int i = 0; i < n; i++) {
                        double vip = gsl_matrix_get(V, i, p);
                        double viq = gsl_matrix_get(V, i, q);
                        gsl_matrix_set(V, i, p, vip * c - s * viq);
                        gsl_matrix_set(V, i, q, viq * c + s * vip);
                    } //Inserting into V

                }
            }
      }

    }while(changed!=0);

return sweep;}


int jacobi_row(gsl_matrix*A,gsl_vector*e,gsl_matrix*V, int m){ //m is number of rows gone through
    int n=A->size1;
    assert(m<=n-1);
    assert(m>0);
    for(int i=0;i<n;i++) {
        gsl_vector_set(e, i, gsl_matrix_get(A, i, i)); //Pick Diagonal elements of A
    }
    gsl_matrix_set_identity(V);
    int sweep=0,p,q,changed;

    //Here we do the jacobi diagonalization A'=J^T*A*J
    //We do it clasically in a row
  for (p = 0; p < m; p++){
    do {
        changed = 0;
        sweep++;

            for (q = p+1; q < n; q++) {
                //THe pp,qq,pq'ed elements in A (we starte in a01 a00 (remains the same))
                double app = gsl_vector_get(e, p); //Used for phi in tan for rotation
                double aqq = gsl_vector_get(e, q); //tan(2*phi)=2*apq/(aqq-app)
                double apq = gsl_matrix_get(A, p, q);

                double phi = atan2(2.0*apq, aqq - app)/2.0; //find phi for rotation matrix

                double c = cos(phi);
                double s = sin(phi);

                double app1 = c * c * app - 2.0 * s * c * apq + s * s * aqq; //App'
                double aqq1 = s * s * app + 2.0 * s * c * apq + c * c * aqq; //Aqq'

                if (app1 != app) {
                    changed = 1; //So loop wont end, we do so it ends when it doesnt change after a sweep because then changed=0
                    gsl_vector_set(e, p, app1);
                    gsl_vector_set(e, q, aqq1);
                    gsl_matrix_set(A, p, q, 0.0);

                    //moving on to Api',Aqi',Aip',Aiq'
                    for (int i = 0; i < p; i++) {
                        double aip = gsl_matrix_get(A, i, p);
                        double aiq = gsl_matrix_get(A, i, q);
                        gsl_matrix_set(A, i, p, aip * c - s * aiq);
                        gsl_matrix_set(A, i, q, aiq * c + s * aip);
                    }

                    for (int i = p + 1; i < q; i++) {
                        double api = gsl_matrix_get(A, p, i);
                        double aiq = gsl_matrix_get(A, i, q);
                        gsl_matrix_set(A, p, i, api * c - s * aiq);
                        gsl_matrix_set(A, i, q, aiq * c + s * api);
                    }

                    for (int i = q + 1; i < n; i++) {
                        double api = gsl_matrix_get(A, p, i);
                        double aqi = gsl_matrix_get(A, q, i);
                        gsl_matrix_set(A, p, i, api * c - s * aqi);
                        gsl_matrix_set(A, q, i, aqi * c + s * api);
                    }

                    for (int i = 0; i < n; i++) {
                        double vip = gsl_matrix_get(V, i, p);
                        double viq = gsl_matrix_get(V, i, q);
                        gsl_matrix_set(V, i, p, vip * c - s * viq);
                        gsl_matrix_set(V, i, q, viq * c + s * vip);
                    } //Inserting into V
                }
            }
        }while(changed!=0);
    }

return sweep;}



































void print_matrix(gsl_matrix * M){
    int n=(*M).size1, m=(*M).size2;
    for (int row=0; row<n; row++)
    {
        for(int columns=0; columns<m; columns++){
            printf("%10.3f     ", gsl_matrix_get(M,row,columns));}
        printf("\n");
    }
}

void print_vector(gsl_vector * M){
    int n=(*M).size;
    for (int row=0; row<n; row++){
        printf("%10.3f     ", gsl_vector_get(M,row));}
    printf("\n");}




