Andreas Hagner - 201406671 - 29/06 2017
__________________________

The exercise for this exam is #14. ODE: a more advanced step-size control.

Which implements the condition for the trial step if,
|dy_k| < t_k = (d+e|y_k|)*sqrt( h/(b-a) ) for k = 1, ..., n.
Meaning that a new Driver for ordinary differential equations has to be implemented.
The Runge-Kutta 45 was taken from the implementation in exercise 8.
