#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

void rk45(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err);

void Advanced_driver(double t, double b, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), double acc, double eps );


void ODE_trig(double x, gsl_vector* y, double *dydx){
    dydx[0] = gsl_vector_get(y,1);
    dydx[1] = (-1.0)*gsl_vector_get(y,0);
}









int main() {

    double h = 0.01, acc = 1e-10, eps = 1e-14; // acc skal være større end 1e-12 ellers er der round of errors og der bliver ganget med nul.
// Part A--------------------------------------------------------------------

    gsl_vector*y = gsl_vector_alloc(2);

    double t = 0.0;
    double b= 2.0;
    gsl_vector_set(y,0, 0.0);
    gsl_vector_set(y,1, 1.0);

    fprintf(stdout,"Exercise 14(A) - ODE: a more advanced step-size control\n");
    fprintf(stdout,"Solving ODEs using the Runge-Kutta45 method with the Advanced-Driver which uses element-wise step acceptance for step correction.\n");
    Advanced_driver(t, b, h, y, ODE_trig, rk45, acc, eps);

    fprintf(stdout,"As an example that it works we solve the trig-ODE y0'=y1 and y1'=-y0 for x=2, \n");
    fprintf(stdout,"The ODE result is y_0= %g, y_1= %g \nExact result is sin(2)= %g, cos(2)= %g.\n", gsl_vector_get(y,0), gsl_vector_get(y,1), sin(b), cos(b) );
    fprintf(stdout,"Which has errors,\nerr(sin(2)-y_0) = %g and err(cos(2)-y_1) = %g.\n",sin(b)-gsl_vector_get(y,0),cos(b)-gsl_vector_get(y,1));
    fprintf(stdout,"With relative and absolute precision,\neps = %g and delta=%g\n",eps,acc);







    gsl_vector_free(y);
return 0;
}
