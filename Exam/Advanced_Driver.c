#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>


void Advanced_driver(double t, double b, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), double acc, double eps ){
    //assert(acc>1e-12);
    int n=y->size;

    double a = t ;

    gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* yvector = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);
    gsl_vector* tau = gsl_vector_alloc(n);
    gsl_vector*  tau_over_e = gsl_vector_alloc(n);


    do{
        double x=t;

        gsl_vector_memcpy(yvector,y);

        if(x+h > b){ h = b-x; }

        stepper(x,h,yvector,f,yh,err); // err is the local error vector


        for(int i=0; i<n; i++){ //Making the tolerance vector tau
            gsl_vector_set( tau, i, (eps*fabs(gsl_vector_get(yh,i)) + acc)*sqrt( h/(b-a) ) );
        }

        //Step_acceptance=1 (set to true), if there exist k such (e_k < t_k) = false, set step_acceptance=0.
        int step_acceptance = 1;
        for(int k=0;k<n;k++){
            double e_k = fabs(gsl_vector_get(err,k));
            double tau_k = gsl_vector_get(tau,k);
            //fprintf(stderr,"%g,  %g\n",tau_k,e_k);
            //check if false
            if( e_k > tau_k){
                step_acceptance = 0;
                gsl_vector_set(tau_over_e, k ,tau_k/e_k ); //for empirical-correction
              }
        }

        //if(step_acceptance!=1){fprintf(stderr,"luder =%i\n",step_acceptance);}


        if( step_acceptance ){
            t = x+h;
            gsl_vector_memcpy(y,yh);
        }
        else{  //I am unsure if there needs to be a condition here, but it works.
            double min_toe = gsl_vector_min(tau_over_e); //New min (t_k/e_k)
            h*=pow( min_toe ,0.25)*0.95;
        }

    }while(t<b);

    gsl_vector_free(yh);
    gsl_vector_free(yvector);
    gsl_vector_free(err);
    gsl_vector_free(tau);
    gsl_vector_free(tau_over_e);
}