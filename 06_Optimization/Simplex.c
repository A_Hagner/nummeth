#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>



void reflection(gsl_vector*high, gsl_vector*centroid,gsl_vector*reflected){
    for(int i=0; i<high->size; i++){
        gsl_vector_set(reflected,i) = 2*gsl_vector_get(centroid,i)-gsl_vector_get(high,i);
    }

}

void expansion(gsl_vector*high, gsl_vector*centroid, gsl_vector*expanded){
    for(int i=0; i<high->size; i++) {
        gsl_vector_set(expanded, i) = 3 * gsl_vector_get(centroid, i) - 2 * gsl_vector_get(high, i);
    }
}

void contraction(gsl_vector*high, gsl_vector*centroid, gsl_vector*contracted){
    for(int i=0; i<high->size; i++) {
        gsl_vector_set(contracted,i) = 0.5*gsl_vector_get(centroid,i) + 0.5*gsl_vector_get(high,i);
    }
}

void reduction(gsl_matrix*simplex, int lo){
    for(k=0; k< 1 + simplex->size1; k++){
        if(k!=lo){
            for(int i=0; i<simplex->size; i++){
                gsl_matrix_set(simplex,k,i, 0.5*( gsl_matrix_get(simplex,k,i) + gsl_matrix_get(simplex,lo,i) )  );
            }
        }
    }
}


double distance(gsl_vector*a, gsl_vector*b){
    double s=0;
    for(int i=0; i<a->size; i++){
        s += pow(gsl_vector_get(b,i)-gsl_vector_get(a,i),2);
    }
    return sqrt(s);
}


double size(gsl_matrix*simplex){
    double s=0;
    for(int i=1; i<1+a->size; i++){
        double dist = distance(si);
    }
    return s;
}
















