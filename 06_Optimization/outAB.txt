Exercise A)
 Newton Minimization of the Himmelblau function
The Himmelblau function has a min at, (x,y) =    -2.805          3.131     
It was found In 19 number of calls
We have shown before that the Himmelblau has 4 local minima, the other 3 will be ignored for now.

Newton Minimization of the Rosenbrock function
The Rosenbrock function has a min at, (x,y) =     1.000          1.000     
It was found In 58 number of calls

Exercise B)
Quasi-Newton Minimization of the Himmelblau function
The Himmelblau function has a min at, (x,y) =    -2.805          3.131     
It was found In 69634 number of calls
It can now be seen that the number of steps for the two method are, quasi-Newton, 69634 > 19, explicit Newton.
 