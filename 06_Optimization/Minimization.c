#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

void backsub(gsl_matrix*A, gsl_vector*c);

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R);

void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x);

int print_matrix(gsl_matrix*M);

int print_vector(gsl_vector*M);


double rosenbrock(gsl_vector*t){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    return (1-x)*(1-x) + 100*pow((y-x*x),2);
}

void gradient_rosenbrock(gsl_vector*t,gsl_vector*df) {
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    gsl_vector_set(df,  0,  -2*(1-x) - 400*x*(y-x*x)  );
    gsl_vector_set(df, 1,  200*(y - x*x) );
}

void hessian_rosenbrock(gsl_vector*t, gsl_matrix*h){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    double dxdx = 2 - 400*y + 1200*x*x ;
    double dxdy = - 400*x;
    double dydy = 200;
    gsl_matrix_set(h,0,0,dxdx);
    gsl_matrix_set(h,1,1,dydy);
    gsl_matrix_set(h,0,1,dxdy);
    gsl_matrix_set(h,1,0,dxdy);
}



double himmelblau(gsl_vector*t){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    return pow(x*x+y-11,2)+pow(x+y*y-7,2);
}


void gradient_himmelblau(gsl_vector*t,gsl_vector*df){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    gsl_vector_set(df,  0,  4*x*(x*x+y-11) + 2*(x + y*y - 7));
    gsl_vector_set(df, 1, 2*(x*x + y - 11) + 4*y*(x+y*y-7) );
}

void hessian_himmelblau(gsl_vector*t, gsl_matrix*h){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    double dxdx = 12*x*x+4*y - 42;
    double dxdy = 4*x + 4*y;
    double dydy = 2 + 4*x + 12*y*y - 28;
    gsl_matrix_set(h,0,0,dxdx);
    gsl_matrix_set(h,1,1,dydy);
    gsl_matrix_set(h,0,1,dxdy);
    gsl_matrix_set(h,1,0,dxdy);
}












void Newton(double f(gsl_vector*x), void gradient(gsl_vector*x, gsl_vector*df), void hessian(gsl_vector*x, gsl_matrix*H), gsl_vector*x, double eps, int* iter){
    int calls=0;

    int n=x->size;
    gsl_matrix* H = gsl_matrix_alloc(n,n);
    gsl_matrix* R = gsl_matrix_alloc(n,n);

    gsl_vector* fx = gsl_vector_alloc(n);
    gsl_vector* xny  = gsl_vector_alloc(n);
    gsl_vector* fxny = gsl_vector_alloc(n);
    gsl_vector* df = gsl_vector_alloc(n);
    gsl_vector* Dx = gsl_vector_alloc(n);

    double Dxtdf=0;
    do{calls++;

        hessian(x,H);
        gradient(x,df);

        //Now sove J*Dx
        qr_gs_decomp(H,R);
        gsl_vector_scale(df,-1.0);
        qr_gs_solve(H,R,df,Dx);
        double s=1.0;


        do{


            s*=0.5;
            gsl_vector_memcpy(xny,x);
            gsl_blas_daxpy(s,Dx,xny);
            gsl_blas_ddot(Dx,df,&Dxtdf);


        }while( fabs(f(xny))> fabs(f(x)) +  0.1*s*Dxtdf);

        gsl_vector_memcpy(x,xny);
        //gsl_vector_memcpy(fx,fxny);

        if(calls>1000000){ fprintf(stderr,"\n Err: Too many calls\n"); break;}
    }while(gsl_blas_dnrm2(df)>eps);

    *iter = calls;

    gsl_matrix_free(H);
    gsl_matrix_free(R);
    gsl_vector_free(fx);
    gsl_vector_free(fxny);
    gsl_vector_free(xny);
    gsl_vector_free(df);
    gsl_vector_free(Dx);
}







void quasi_Newton(double f(gsl_vector*x), void gradient(gsl_vector*x, gsl_vector*df), gsl_vector*x, double eps, double dx, int* iter){
    int calls=0;

    int n=x->size;
    gsl_matrix* H = gsl_matrix_alloc(n,n);
    gsl_matrix* dH = gsl_matrix_alloc(n,n);
    gsl_matrix* Hinv = gsl_matrix_alloc(n,n);
    gsl_matrix_set_identity(Hinv);


    gsl_matrix* R = gsl_matrix_alloc(n,n);
    gsl_matrix* outerprod = gsl_matrix_alloc(n,n);

    gsl_vector* xny  = gsl_vector_alloc(n);
    gsl_vector* dfxny = gsl_vector_alloc(n);
    gsl_vector* df = gsl_vector_alloc(n);
    gsl_vector* Dx = gsl_vector_alloc(n);
    gsl_vector* c = gsl_vector_alloc(n);
    gsl_vector* y = gsl_vector_alloc(n);
    gsl_vector* s = gsl_vector_alloc(n);
    gsl_vector* Hinv_y = gsl_vector_alloc(n);
    gsl_vector* smHinv_y = gsl_vector_alloc(n);
    gsl_vector* Hinv_s =gsl_vector_alloc(n);

    double yT_Hinv_s=0;

    double Dxtdf=0;

    double fx = f(x);
    double fxny = f(xny);
    gradient(x,df);


    do{


        gsl_blas_dgemv(CblasNoTrans, 1.0, Hinv, df, 0, Dx);
        gsl_vector_scale(Dx,-1.0);

        //Now sove J*Dx
        gsl_vector_memcpy(s,Dx);
        gsl_vector_scale(s,2); // we do this so the first time gives s.



        do{calls++;
            gsl_vector_scale(s,0.5);

            gsl_vector_memcpy(xny,x);

            gsl_vector_add(xny,s);
            fxny = f(xny);

            gsl_blas_ddot(s,df,&Dxtdf);

            if(gsl_blas_dnrm2(s) < dx ){gsl_matrix_set_identity(Hinv);}
        }while( fabs( fxny ) > fabs(fx) + 0.1*Dxtdf && gsl_blas_dnrm2(s) > dx );

        gradient(xny, dfxny);

        gsl_vector_memcpy(y,dfxny);

        gsl_vector_sub(y,df); // y = df(x) - df(x+s)

        gsl_blas_dgemv(CblasNoTrans, 1.0, Hinv, y, 0, Hinv_y);


        // Now calculate dH --->  ( (s - Hinv * y) sT Hinv ) / ( yT H inv s )
        gsl_vector_memcpy( smHinv_y ,s);

        gsl_vector_sub(smHinv_y, Hinv_y); // s- Hinv*y  in smHninv_y



        gsl_blas_dgemv(CblasNoTrans, 1.0, Hinv, s, 0, Hinv_s); //H⁻¹ * s


        // (s - Hinv * y) sT Hinv  - is an outer product and is therefore a matrix with entries A_ij = V_i * U_j
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                gsl_matrix_set(outerprod,i,j, gsl_vector_get(smHinv_y,i)*gsl_vector_get(Hinv_s,j) ); // We can use Hinv_s since the elements are the same as for sT*Hinv
            }
        }


        gsl_blas_ddot(y,Hinv_s, &yT_Hinv_s);     // bottom of update matrix is just a scalar since quadratic form

        gsl_matrix_scale(outerprod,1.0/yT_Hinv_s );

        gsl_matrix_add(Hinv,outerprod);

        gsl_vector_memcpy(x,xny);
        gsl_vector_memcpy(df,dfxny);
        fx=fxny;


        if(calls>1000000){ fprintf(stderr,"\n Err: Too many calls\n"); break;}
    }while(gsl_blas_dnrm2(df)>eps);

    *iter = calls;

    gsl_matrix_free(H);
    gsl_matrix_free(dH);
    gsl_matrix_free(Hinv);
    gsl_matrix_free(outerprod);
    gsl_matrix_free(R);

    gsl_vector_free(xny);
    gsl_vector_free(df);
    gsl_vector_free(Dx);
    gsl_vector_free(c);
    gsl_vector_free(y);
    gsl_vector_free(s);

    gsl_vector_free(Hinv_s);
    gsl_vector_free(Hinv_y);
    gsl_vector_free(smHinv_y);
}
