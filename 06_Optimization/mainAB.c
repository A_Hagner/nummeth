#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>


int print_matrix(gsl_matrix * M);

int print_vector(gsl_vector * M);

void Newton(double f(gsl_vector*x), void gradient(gsl_vector*x, gsl_vector*df), void hessian(gsl_vector*x, gsl_matrix*H), gsl_vector*x, double eps, int* iter);

void quasi_Newton(double f(gsl_vector*x), void gradient(gsl_vector*x, gsl_vector*df), gsl_vector*x, double eps, double dx, int* iter);




double himmelblau(gsl_vector*t);

void gradient_himmelblau(gsl_vector*t,gsl_vector*df);

void hessian_himmelblau(gsl_vector*t, gsl_matrix*h);



double rosenbrock(gsl_vector*t);

void gradient_rosenbrock(gsl_vector*t,gsl_vector*df);

void hessian_rosenbrock(gsl_vector*t, gsl_matrix*h);




int main(){

    int Himmel_calls = 0;
    gsl_vector*x = gsl_vector_calloc(2);
    gsl_vector_set(x,0, -2.6);
    gsl_vector_set(x,1, 3.0 );
    double eps = 10e-5;


    Newton(himmelblau, gradient_himmelblau, hessian_himmelblau, x, eps, &Himmel_calls);

    fprintf(stdout,"Exercise A)\n Newton Minimization of the Himmelblau function\n");
    fprintf(stdout,"The Himmelblau function has a min at, (x,y) =");
    print_vector(x);
    fprintf(stdout,"It was found In %i number of calls\n",Himmel_calls);
    fprintf(stdout,"We have shown before that the Himmelblau has 4 local minima, the other 3 will be ignored for now.\n\n");



    int Rosen_calls = 0;

    Newton(rosenbrock, gradient_rosenbrock, hessian_rosenbrock, x, eps, &Rosen_calls);

    fprintf(stdout,"Newton Minimization of the Rosenbrock function\n");
    fprintf(stdout,"The Rosenbrock function has a min at, (x,y) =");
    print_vector(x);
    fprintf(stdout,"It was found In %i number of calls\n",Rosen_calls);

    fprintf(stderr," \n \n");

    //___________________________________________ Exercise B

    int R = 0;
    double dx = 0.01;

    gsl_vector_set(x,0, -2.6 );
    gsl_vector_set(x,1,  3.0 );

	fprintf(stdout,"\nExercise B)\n");
    fprintf(stdout,"Quasi-Newton Minimization of the Himmelblau function\n");

    quasi_Newton(himmelblau,gradient_himmelblau, x, eps, dx, &R);

    fprintf(stdout,"The Himmelblau function has a min at, (x,y) =");
    print_vector(x);
    fprintf(stdout,"It was found In %i number of calls\n",R);

    fprintf(stdout,"It can now be seen that the number of steps for the two method are, quasi-Newton, %i > %i, explicit Newton.\n ",R,Himmel_calls);



    double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
    double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
    double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
    int N = sizeof(t)/sizeof(t[0]);


































return 0;}
