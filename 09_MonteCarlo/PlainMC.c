//
// Created by hagner on 6/26/17.
//

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

#define RND (double)rand()/RAND_MAX

void rando(int dim, double a[],double b[], double*x){
    for(int i=0; i<dim; i++){
        x[i] = a[i] + RND*(b[i]-a[i]);
    }
}

//OG MC IN DA HIPPITY HOUSE
void Ice_Cube(int dim, double a[], double b[], double f(double*x), int N, double*result, double*error){
    double V=1;
    double sum=0, sums=0;
    for(int i=0; i<dim; i++){
    V*=b[i]-a[i];
    }

    double x[dim];
    for(int i=0; i<N; i++){
        rando(dim, a, b, x);
        sum += f(x);
        sums += f(x)*f(x);
    }
    double average = sum/N;
    *result = average*V;
    *error = sqrt((sums/N-average*average)/N)*V;
}