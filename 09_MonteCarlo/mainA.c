//
// Created by hagner on 6/26/17.
//

#include<stdio.h>
#include<stdlib.h>
#include<math.h>

void Ice_Cube(int dim, double*a, double*b, double f(double*x), int N, double*result, double*error);



double f_pi(double x[]){
	return 4.0*sqrt(1.0-pow(1.0-x[0],2));
}

double f_a(double x[]){
    return pow((M_PI),-3)*1.0/(1.0-cos(x[0])*cos(x[1])*cos(x[2]));
}



int main(){
    double result=0;
    double error=0;
    double a[]={0,0,0};
    double b[]={M_PI,M_PI,M_PI};
    int N= 10000000;
    Ice_Cube(3,a,b,f_a, N, &result, &error);
    fprintf(stdout,"Exercise A)\n Plain Monte-Carlo integration\n");
    fprintf(stdout,"The integral of π⁻³*(1-cos(x)cos(y)cos(z))⁻¹ over 0 to π in 3D.\n");
    fprintf(stdout,"The Plain Monte-Carlo integral gives: Γ(1/4)⁴/(4π³) = %g,  error=%g\n",result,error);
    fprintf(stdout,"Exact result is: Γ(1/4)⁴/(4π³) = %g\n",1.3932039296856768591842462603255);


}











