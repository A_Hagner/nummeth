#include<stdio.h>
#include<stdlib.h>
#include<math.h>



void Ice_Cube(int dim, double a[], double b[], double f(double*x), int N, double*result, double*error);


double f_pi(double x[]){
    return 4.0*sqrt(1.0-pow(1.0-x[0],2));
}


int main(){

    fprintf(stderr,"\nExercise B)\nTest of error behaviour of the plain Monte-Carlo method.\n");
    fprintf(stderr,"The error should behave as: O(1/sqrt(N)) (where N is the number of evaluations).\n");
    fprintf(stderr,"Please see plot for comparison\n\n");
    int N=0;
    double a[]={0};
    double b[]={1};

    fprintf(stdout,"N\te\tO\n");
    for(int i=0;i<100;i++){
        N+=10000;
        double result=0,error=0;
        Ice_Cube(1,a,b,f_pi,N,&result,&error);
        fprintf(stdout,"%i\t%g\t%g\n",N,error,pow(N,-0.5));
    }


}
