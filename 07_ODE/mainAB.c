#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

void rk12(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err);

void rk45(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err);

void human_bean(double t, double b, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), double acc, double eps );

void ODE_trig(double x, gsl_vector* y, double *dydx){
    dydx[0] = gsl_vector_get(y,1);
    dydx[1] = (-1.0)*gsl_vector_get(y,0);
}

void ODE_logistic(double x, gsl_vector*y, double*dydx){
    dydx[0]= gsl_vector_get(y,0)*(1.0 - gsl_vector_get(y,0));
}

void Ryan_gosling( gsl_vector* t_step, gsl_matrix* y_step, double b, double h, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), int*steps, int max, double acc, double eps);


int print_matrix(gsl_matrix * M){
    int n=(*M).size1, m=(*M).size2;
    for (int row=0; row<n; row++)
    {
        for(int columns=0; columns<m; columns++){
            printf("%10.3f     ", gsl_matrix_get(M,row,columns));}
        printf("\n");
    }
    return 0;
}

int print_vector(gsl_vector * M){
    int n=(*M).size;
    for (int row=0; row<n; row++){
        printf("%10.3f \n", gsl_vector_get(M,row));}
    printf("\n");
    return 0;
}











int main() {

    double h = 0.01, acc = 1e-6, eps = 1e-6;
// Part A--------------------------------------------------------------------

    gsl_vector*y = gsl_vector_alloc(2);
    gsl_vector*yl = gsl_vector_alloc(1);
    double t = 0.0;
    double b= 2;
    gsl_vector_set(y,0, 0.0);
    gsl_vector_set(y,1, 1.0);

    fprintf(stdout,"Exercise A) \n Solving ODEs using the Runge-Kutta45 method \n");
    human_bean(t, b, h, y, ODE_trig, rk45, acc, eps);

    fprintf(stdout,"Solving the trig-ODE y0'=y1 and y1'=-y0 for x=2, \n");
    fprintf(stdout," The ODE result is y_0= %g, y_1= %g \n Exact result is sin(2)= %g, cos(2)= %g\n", gsl_vector_get(y,0), gsl_vector_get(y,1), sin(b), cos(b) );

    gsl_vector_set(yl,0, 0.5);
    fprintf(stdout,"Solving the Logistic-ODE for x=3 \n");
    human_bean(0.0, 3.0, 0.1, yl, ODE_logistic, rk45, acc, eps);
    fprintf(stdout, " The ODE result is y0= %g, \n Exact result is logistic(3) = 0.952574 \n",gsl_vector_get(yl,0));


// - - - - - - - - - - - - - Part B  - - - - - - - - - - - - - - - - - - - -

    int max=15;
    int dim = 2;
    gsl_matrix* y_step = gsl_matrix_alloc(max,dim);
    gsl_vector* t_step = gsl_vector_alloc(max);

    //b=2.0;

    gsl_vector_set(t_step,0, 0.0);

    gsl_matrix_set(y_step, 0, 0, 0.0);
    gsl_matrix_set(y_step, 0, 1, 1.0);


    int steps=0;

    Ryan_gosling(t_step, y_step, 2.0, 0.01, ODE_trig, rk45, &steps, max, acc, eps);


    fprintf(stdout,"\n\nExercise B) \nODE-Driver which stores the path.\n");
    fprintf(stdout,"Solving the trig-ODE as before.\n");
    fprintf(stdout,"The steps in t,\n");
    print_vector(t_step);
    fprintf(stdout,"The steps in y,\n");
    print_matrix(y_step);














return 0;
}
