//
// Created by hagner on 6/23/17.
//
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

const double l[] = {0, 1.0/4.0, 3.0/8.0, 12.0/13.0, 1.0, 1.0/2.0};

const double r1[] = {1.0/4.0};
const double r2[] = {3.0/32.0, 9.0/32.0};
const double r3[] = {1932.0/2197.0,-7200.0/2197.0,7296.0/2197.0};
const double r4[] = {439.0/216.0, -8.0,  3680.0/513, -845.0/4104.0};
const double r5[] = {-8.0/27.0, 2.0, -3544.0/2565.0, 1859.0/4104.0, -11.0/40.0};

const double b1[] = {16.0/135.0, 0.0, 6656.0/12825.0, 28561.0/56430.0, -9.0/50.0, 2.0/55.0};
const double b2[] = {25.0/216.0, 0.0, 1408.0/2565.0, 2197.0/4104.0, -1.0/5.0, 0};




void rk45(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err){
    int n=y->size;
    double k1[n],k2[n],k3[n],k4[n],k5[n],k6[n];

    gsl_vector*yt = gsl_vector_alloc(n);


    f(t+l[0]*h,y,k1);

    for(int i=0; i<n; i++){
        gsl_vector_set(yt,i, gsl_vector_get(y,i)+r1[0]*k1[i]*h);
    }


    f(t+l[1]*h,yt,k2);

    for (int i = 0; i < n; ++i) {
        gsl_vector_set(yt,i, gsl_vector_get(y,i)+h*(r2[0]*k1[i]+r2[1]*k2[i] ) );
    }


    f(t+l[2]*h,yt,k3);

    for (int i = 0; i < n; ++i) {
        gsl_vector_set(yt,i, gsl_vector_get(y,i)+h*(r3[0]*k1[i]+r3[1]*k2[i] + r3[2]*k3[i]) );
    }

    f(t+l[3]*h,yt,k4);

    for (int i = 0; i < n; ++i) {
        gsl_vector_set(yt,i, gsl_vector_get(y,i)+h*( r4[0]*k1[i]+r4[1]*k2[i] + r4[2]*k3[i] + r4[3]*k4[i] ) );
    }

    f(t+l[4]*h,yt,k5);

    for (int i = 0; i < n; i++) {
        gsl_vector_set(yt,i, gsl_vector_get(y,i)+h*( r5[0]*k1[i]+r5[1]*k2[i] + r5[2]*k3[i] + r5[3]*k4[i] + r5[4]*k5[i] ) );
    }

    f(t+l[5]*h,yt,k6);


    //4-order
    for(int i=0; i<n; i++){
        double yhtmp = gsl_vector_get(y,i) + h*(b1[0]*k1[i] + b1[1]*k2[i] + b1[2]*k3[i] + b1[3]*k4[i] + b1[4]*k5[i] + b1[5]*k6[i]) ;
        double y4 = gsl_vector_get(y,i) + h*(b2[0]*k1[i] + b2[1]*k2[i] + b2[2]*k3[i] + b2[3]*k4[i] + b2[4]*k5[i] + b2[5]*k6[i]) ;
        gsl_vector_set(err,i,yhtmp -  y4);
        gsl_vector_set(yh,i, yhtmp );
    }

    gsl_vector_free(yt);
}


/*
void rk12(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err){
    int n=y->size;
    double k0[n];
    double k12[n];
    gsl_vector*yt = gsl_vector_alloc(n);

    f(t,y,k0);
    for(int i=0;i<n;i++){
        gsl_vector_set(yt,i,gsl_vector_get(y,i)+k0[i]*h/2);
    }
    f(t+h/2,yt,k12);
    for(int i=0; i<n; i++){
        gsl_vector_set(yh,i,gsl_vector_get(y,i)+k12[i]*h);
    }
    for(int i=0; i<n; i++){
        gsl_vector_set(err,i, k0[i]-k12[i]*h/2);
    }
}*/




// AKA Ryan Gosling AKAKA A REAL HUMAN BEING/BEAN AKAKAKA Manden der skal se godt ud og holde kæft AKAKAKAKA Driver
void human_bean(double t, double b, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), double acc, double eps ){

    int n=y->size;
    //int k=0;
    double a = t ;

    gsl_vector* yh = gsl_vector_alloc(n);
    gsl_vector* yvector = gsl_vector_alloc(n);
    gsl_vector* err = gsl_vector_alloc(n);







    double y_norm;
    double error;

do{
    double x=t;


    gsl_vector_memcpy(yvector,y);

    if(x+h > b){h = b-x; }

    stepper(x,h,yvector,f,yh,err);

    double s=0;
    for(int i=0; i<n; i++){
        s += gsl_vector_get(err,i)*gsl_vector_get(err,i);
        error = sqrt(s);
    }


    double tau=0;
    for(int i=0; i<n; i++){
        tau += gsl_vector_get(yh,i)*gsl_vector_get(yh,i);
        y_norm = sqrt(tau);
    }


    double tolerance = (y_norm*eps+acc)*sqrt(h/(b-a));

    if(error < tolerance){//REJECT if fulfilled
        t = x+h;
        gsl_vector_memcpy(y,yh);
    }
    if(error > 0){
        h*=pow(tolerance/error,0.25)*0.95;
    }
    else{h*=2;}

}while(t<b);

    gsl_vector_free(yh);
    gsl_vector_free(yvector);
    gsl_vector_free(err);
}




void Ryan_gosling(gsl_vector* t_step, gsl_matrix* y_step, double b, double h, void f(double t, gsl_vector*y, double*dydt), void stepper(double t, double h, gsl_vector*y, void f(double t, gsl_vector*y, double*dydt), gsl_vector*yh, gsl_vector* err), int*steps, int max, double acc, double eps){
    int k=0;
    int dim = y_step->size2;

    gsl_vector*yvector = gsl_vector_alloc(dim);
    gsl_vector*yh = gsl_vector_alloc(dim);
    gsl_vector*err = gsl_vector_alloc(dim);

    double a = gsl_vector_get(t_step,0);


    do{
        double t = gsl_vector_get(t_step,k);

        for(int i=0; i < dim; i++ ){
            gsl_vector_set(yvector, i , gsl_matrix_get(y_step, k, i)  );
        }

        //fprintf(stderr,"t=%g\n",t);
        if( t+h > b){ h = b-t;}


        stepper(t, h, yvector, f, yh, err);

        double s=0;
        for(int i=0; i<dim; i++){
            s += gsl_vector_get(err,i)*gsl_vector_get(err,i);
        }
        double error = sqrt(s);

        double tau=0;
        for(int i=0; i<dim; i++){
            tau += gsl_vector_get(yh,i)*gsl_vector_get(yh,i);
        }
        double y_norm = sqrt(tau);


        double tolerance = (y_norm*eps+acc)*sqrt(h/(b-a));


        if(error < tolerance){
            k++;
            if(k> max-1){fprintf(stderr,"\n Does not converge \n");
                         break;   }


            gsl_vector_set(t_step, k, t+h);


            gsl_matrix_set_row(y_step,k,yh);


            gsl_vector_memcpy(yvector,yh);

        }
        if( error > 0.0 ){
            h*=pow(tolerance/error,0.25)*0.95;
        }
        else{h*=2;}


    }while(gsl_vector_get(t_step,k) < b );

    *steps = k;

    gsl_vector_free(yh);
    gsl_vector_free(yvector);
    gsl_vector_free(err);

}



























