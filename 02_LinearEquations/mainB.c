#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

void backsub(gsl_matrix*A, gsl_vector*c);

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R);

void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x);

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B);

int print_matrix(gsl_matrix*M);

int print_vector(gsl_vector*M);

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B);

int main(){

    srand(time(NULL));

    size_t k=4;
    gsl_matrix*A= gsl_matrix_alloc(k,k);
    gsl_matrix*AA= gsl_matrix_alloc(k,k);
    gsl_matrix*R= gsl_matrix_alloc(k,k);
    gsl_matrix*B= gsl_matrix_alloc(k,k);
    gsl_matrix*I= gsl_matrix_alloc(k,k);



    for(int i=0;i<k;i++){
        for(int j=0;j<k;j++) {
            double X = 5 * ((double) rand() / (double) RAND_MAX);
            gsl_matrix_set(A, i, j, X);
            gsl_matrix_set(AA, i, j, X);

        }
    }

    qr_gs_decomp(A,R);
    qr_gs_inverse(A,R,B);

    gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,AA,B,0.0,I);
    printf("Matrix A*A^(-1) (should be Identity)\n");
    print_matrix(I);





gsl_matrix_free(A);
gsl_matrix_free(AA);
gsl_matrix_free(R);
gsl_matrix_free(B);
gsl_matrix_free(I);
}








