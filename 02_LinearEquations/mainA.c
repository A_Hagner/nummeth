#include<stdio.h>
#include<math.h>
#include<time.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

void backsub(gsl_matrix*A, gsl_vector*c);

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R);

void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x);

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B);

int print_matrix(gsl_matrix*M);

int print_vector(gsl_vector*M);


int main(){

    srand(time(NULL));

    size_t n=4, m=3;
    gsl_matrix*A= gsl_matrix_alloc(n,m);
    gsl_matrix*R= gsl_matrix_alloc(m,m);

    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++) {
            double X = 5 * ((double) rand() / (double) RAND_MAX);
            gsl_matrix_set(A, i, j, X);
        }
    }

    //Check decomp

    printf("Matrix A \n");
    print_matrix(A);

    qr_gs_decomp(A,R);

    printf("Matrix Q \n");
    print_matrix(A);

    printf("Matrix R \n");
    print_matrix(R);


    gsl_matrix *  QTQ=gsl_matrix_calloc (m,m);
    gsl_blas_dgemm (CblasTrans,CblasNoTrans, 1.0,A,A,0.0,QTQ);// calculates 1*A^T*A+0*QTQ
    printf("Matrix Q^T*Q \n ");
    print_matrix(QTQ);

    printf("Matrix Q*R \n ");
    gsl_matrix*QR= gsl_matrix_alloc(n,m);
    gsl_blas_dgemm (CblasNoTrans,CblasNoTrans, 1.0,A,R,0.0,QR);// calculates 1*A*R+0*QTQ
    print_matrix(QR);


    //Check Solve
    size_t k=4;
    gsl_matrix*B= gsl_matrix_alloc(k,k);
    gsl_matrix*Q2= gsl_matrix_alloc(k,k);
    gsl_matrix*R2= gsl_matrix_alloc(k,k);
    gsl_vector*b=gsl_vector_alloc(k);
    gsl_vector*x=gsl_vector_alloc(k);
    gsl_vector*s=gsl_vector_alloc(k);


    for(int i=0;i<k;i++){
        for(int j=0;j<k;j++) {
            double X = 5 * ((double) rand() / (double) RAND_MAX);
            double Y = ((double) rand() / (double) RAND_MAX);
            gsl_matrix_set(B, i, j, X);
            gsl_matrix_set(Q2, i, j, X);
            gsl_vector_set(b,i,Y);
        }
    }
    printf("Matrix A2\n");
    print_matrix(B);


    qr_gs_decomp(Q2,R2);

    printf("Matrix Q2\n");
    print_matrix(Q2);

    printf("Matrix R2\n");
    print_matrix(R2);

    qr_gs_solve(Q2,R2,b,x);

    printf("Vector x \n");
    print_vector(x);

    printf("Vector A*x (should be equal to b) \n");
    gsl_blas_dgemv(CblasNoTrans,1.0,B,x,0.0, s);
    print_vector(s);

    printf("Vector b\n");
    print_vector(b);

    gsl_vector_free(s);
    gsl_vector_free(b);
    gsl_vector_free(x);
    gsl_matrix_free(A);
    gsl_matrix_free(R);
    gsl_matrix_free(Q2);
    gsl_matrix_free(R2);
    gsl_matrix_free(B);

}










