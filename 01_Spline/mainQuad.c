#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <assert.h>
#include <asm/errno.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;

qspline* qspline_alloc(int n,double* x, double* y);

double qspline_eval(qspline *s, double z);

double quadinteg(qspline *s, double z);

double quadderiv(qspline *s, double z);


void qspline_free(qspline *s);

int main(int argc, char** argv){		 /*int argc, char** argv*/
    int n=0,nmax=100;
    double x[nmax],y[nmax];
    double a,b;


fscanf(stdin,"%lg %lg",&a,&b);
    do{
        x[n]=a;
        y[n]=b;
        n++;
        if(n>=nmax){return E2BIG;}
    }while( fscanf(stdin,"%lg %lg",&a,&b) != EOF);
	
    qspline* s=qspline_alloc(n, x, y);



    int i;
    double z=0;
	printf("z\teval\treval\tdiff\trdiff\tinteg\trinteg\n");

    for(i=0;i<=50;i++){
        double fz=qspline_eval(s,z+=.06);
        double diff=quadderiv(s,z);
        double integral=quadinteg(s,z);
        printf("%g\t%g\t%g\t%g\t%g\t%g\t%g\n",z,fz,sin(z),diff,cos(z),integral,-cos(z)+cos(0));
    }
	qspline_free(s);

return 0;}







