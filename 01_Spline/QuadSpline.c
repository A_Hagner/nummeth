#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include <assert.h>

typedef struct {int n; double *x, *y, *b, *c;} qspline;


qspline* qspline_alloc(int n,double* x, double* y){
    qspline *s= (qspline*) malloc(sizeof(qspline)); //This is the spline

    s->n=n;
    s->b = (double*) malloc((n-1)*sizeof(double));
    s->c = (double*) malloc((n-1)*sizeof(double));
    s->x = (double*) malloc((n)*sizeof(double));
    s->y = (double*) malloc((n)*sizeof(double));

for(int i=0;i<n;i++){
    s->x[i]=x[i];
    s->y[i]=y[i];
}
    double p[n-1],d[n-1];

for(int i=0; i<n-1; i++){
    d[i]=x[i+1]-x[i];       //d = delta x
    p[i]=(y[i+1]-y[i])/d[i]; // P = delta y / delta x
}
    s->c[0]=0; //Chose c1=0

    //Recusion upward
for(int i=0; i<n-2;i++){
    double cd=s->c[i]*d[i];
    s->c[i+1]=(p[i+1]-p[i]-cd)/d[i+1];
}

    s->c[n-2]/=2; //new start

    //Recursion downward
for(int i=n-3;i>0;i--){
    double cd=s->c[i+1]*d[i+1];
    s->c[i]=(p[i+1]-p[i]-cd)/d[i]; //starter med at bruge c[n-2] og laver ny fra det.
}
//indsætter b
for(int i=0;i<n-1;i++){
    double cd=s->c[i]*d[i];
    s->b[i]=p[i]-cd;
}

return s;}

double qspline_eval(qspline *s, double z){

    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int i=0, j=s->n-1;

    while(j-i>1) {
        int m = (i + j) / 2;
        if (z > s->x[m]) { i = m; }
        else { j = m; }
    }


    double h=z-s->x[i];
    double b=s->b[i];
    double c=s->c[i];
return s->y[i]+h*(b+h*c);}

double quadinteg(qspline *s, double z){
    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int k, i=0, j=s->n-1;


    while(j-i>1) {
        int m = (i + j) / 2;
        if (z > s->x[m]) { i = m; }
        else { j = m; }
    }


    double integral[s->n];
    for(k=0;k<=i-1;k++) {
        double xi=s->x[k];
        double xii=s->x[k+1];
        double yi=s->y[k];
        double yii=s->y[k+1];
        double ci=s->c[k];
        double bi=s->b[k];
        double ai=yi;
        integral[k]= ci*(pow(xii-xi,3))/3+bi*(pow(xii-xi,2))/2+ai*(xii-xi) ;
        //fprintf(stderr,"%i\t%g\t%g\n",k,s->x[k],integral[k]);
    }
    integral[k]=s->c[k]*(pow(z-s->x[k],3))/3+s->b[k]*(pow(z-s->x[k],2))/3+(s->y[k])*(z - s->x[k]) ;;
    //fprintf(stderr,"%i\t%g\t%g\n",k,z,integral[k]);



    int l;
    double sum=0;
    for(l=0; l<=k;l++){

        sum+=integral[l];
        //fprintf(stderr,"%i\t%g\n",l,sum);
    }



    return  sum;}


double quadderiv(qspline *s, double z){
    assert(z>=s->x[0] && z<=s->x[s->n-1]);
    int k, i=0, j=s->n-1;


    while(j-i>1) {
        int m = (i + j) / 2;
        if (z > s->x[m]) { i = m; }
        else { j = m; }
    }

    double diff=s->c[i]*2*(z-s->x[i])+s->b[i];

    return  diff;}







void qspline_free(qspline *s){
    free(s->x);
    free(s->y);
    free(s->b);
    free(s->c);
    free(s);
}



/*
int main() {
    int size=9;
    double x[]={0, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0};
    double y[]={0, sin(0.25), sin(0.5), sin(0.75), sin(1.0), sin(1.25), sin(1.5), sin(1.75), sin(2.0)};

    qspline* sinus;
    sinus = qspline_alloc(size, x, y);
    double z=1.1;
    double ss=qspline_eval(sinus,z);
    printf("sin(1.1)=%g \n",ss);

    qspline_free(sinus);
}
*/
