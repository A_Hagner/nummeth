#include<stdio.h>
#include<math.h>
#include <assert.h>


double lineinterp(int n,double * x, double* y, double z){
    assert(n>1 && z>=x[0] && z<=x[n-1]);
    int i=0, j=n-1;

    while(j-i>1) {
        int m = (i + j) / 2;
        if (z > x[m]) { i = m; }
        else { j = m; }
    }
return y[i]+(y[i+1]-y[i])/(x[i+1]-x[i])*(z-x[i]);}


double lineinteg(int n, double*x, double*y, double z){
    int k, i=0, j=n-1;

    while(j-i>1) {
        int m = (i + j) / 2;
        if (z > x[m]) { i = m; }
        else { j = m; }
    }

    double integral[n];

    for(k=0;k<=i-1;k++) {
        double ai=(y[k+1]-y[k])/(x[k+1]-x[k]);
        double bi=y[k]-ai*x[k];
        integral[k]=0.5 * ai * (x[k + 1] * x[k + 1] - x[k] * x[k])+bi*(x[k+1]-x[k]);
    }
        integral[k+1]=0.5*(y[i+1]-y[i])/(x[i+1]-x[i])*(pow(z,2) - x[i] * x[i])+x[i]*(z-x[i]);

    int l;
    double sum=0;
    for(l=0; l<=k+1;l++){
        sum+=integral[l];
    }



return  sum;}

/*
int main() {
    int size=9;
    double x[]={0, 0.25, 0.5, 0.75, 1.0,1.25,1.5,1.75,2.0};
    double y[]={0, sin(0.25), sin(0.5), sin(0.75), sin(1.0), sin(1.25), sin(1.5), sin(1.75), sin(2.0)};
    double s4=lineinterp(size,x,y,0.25);
    printf("sin(1.1)=%g\n",s4);
    return 0;
}
*/ 

