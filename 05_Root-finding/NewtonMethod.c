#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>



void backsub(gsl_matrix*A, gsl_vector*c);

void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R);

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B);

int print_matrix(gsl_matrix * M);

int print_vector(gsl_vector * M);



void function_one(gsl_vector*t,gsl_vector*df){
    double A=10000.0;
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    gsl_vector_set(df,  0,  A*x*y-1.0);
    gsl_vector_set(df, 1, exp(-x)+exp(-y)-1.0-1.0/A);
}


void rosenbrock(gsl_vector*t,gsl_vector*df) {
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    gsl_vector_set(df,  0,  -2*(1-x) - 400*x*(y-x*x)  );
    gsl_vector_set(df, 1,  200*(y - x*x) );
}

void himmelblau(gsl_vector*t,gsl_vector*df){
    double x = gsl_vector_get(t,0);
    double y = gsl_vector_get(t,1);
    gsl_vector_set(df,  0,  4*x*(x*x+y-11) + 2*(x + y*y - 7));
    gsl_vector_set(df, 1, 2*(x*x + y - 11) + 4*y*(x+y*y-7) );
}

void jacobi_himmelblau(gsl_vector*t, gsl_matrix*j){
        double x = gsl_vector_get(t,0);
        double y = gsl_vector_get(t,1);
        double dxdx = 12*x*x+4*y - 42;
        double dxdy = 4*x + 4*y;
        double dydy = 2 + 4*x + 12*y*y - 28;
        gsl_matrix_set(j,0,0,dxdx);
        gsl_matrix_set(j,1,1,dydy);
        gsl_matrix_set(j,0,1,dxdy);
        gsl_matrix_set(j,1,0,dxdy);
}



void NewtonA(void f(gsl_vector*x,gsl_vector*fx), gsl_vector*x, double dx, double eps, int* iter){
    int calls=0;

    int n=x->size;
    gsl_matrix* J = gsl_matrix_alloc(n,n);
    gsl_matrix* R = gsl_matrix_alloc(n,n);
    gsl_vector* fx = gsl_vector_alloc(n);
    gsl_vector* z  = gsl_vector_alloc(n);
    gsl_vector* fz = gsl_vector_alloc(n);
    gsl_vector* df = gsl_vector_alloc(n);
    gsl_vector* Dx = gsl_vector_alloc(n);

    do{


        f(x,fx);
        for(int i=0;i<n;i++){ // Jacobian
            gsl_vector_set(x,i,gsl_vector_get(x,i)+dx);
            f(x,df);
            gsl_vector_sub(df,fx);
                for(int j=0;j<n;j++){
                    gsl_matrix_set(J,j,i,gsl_vector_get(df,j)/dx);
                }
            gsl_vector_set(x,i,gsl_vector_get(x,i)-dx);
            gsl_vector_set(z,i,gsl_vector_get(x,i));
        }
            //Now sove J*Dx
        qr_gs_decomp(J,R);
        qr_gs_solve(J,R,fx,Dx);
        gsl_vector_scale(Dx,-1);
        double s=1;

        do{ calls++;
            gsl_vector_memcpy(z,x);
            gsl_vector_add(z,Dx);
            f(z,fz);
            s*=0.5;
            gsl_vector_scale(Dx,0.5);
        }while(gsl_blas_dnrm2(fz)>(1-s/2)*gsl_blas_dnrm2(fx) && s>0.02 );

        gsl_vector_memcpy(x,z);
        gsl_vector_memcpy(fx,fz);

    }while(gsl_blas_dnrm2(Dx)>dx && gsl_blas_dnrm2(fx)>eps );

    *iter = calls;

    gsl_matrix_free(J);
    gsl_matrix_free(R);
    gsl_vector_free(fx);
    gsl_vector_free(fz);
    gsl_vector_free(z);
    gsl_vector_free(df);
    gsl_vector_free(Dx);
}




void NewtonB(void f(gsl_vector*x,gsl_vector*fx), gsl_vector*x, void jacobi(gsl_vector*x, gsl_matrix*J), double dx, double eps, int*iter){
    int calls=0;

    int n=x->size;
    gsl_matrix* J = gsl_matrix_alloc(n,n);
    gsl_matrix* R = gsl_matrix_alloc(n,n);
    gsl_vector* fx = gsl_vector_alloc(n);
    gsl_vector* z  = gsl_vector_alloc(n);
    gsl_vector* fz = gsl_vector_alloc(n);
    gsl_vector* df = gsl_vector_alloc(n);
    gsl_vector* Dx = gsl_vector_alloc(n);

    do{
        f(x,fx);
        jacobi(x,J);

        //Now sove J*Dx
        qr_gs_decomp(J,R);
        qr_gs_solve(J,R,fx,Dx);
        gsl_vector_scale(Dx,-1);
        double s=1;

        do{ calls++;
            gsl_vector_memcpy(z,x);
            gsl_vector_add(z,Dx);
            f(z,fz);
            s*=0.5;
            gsl_vector_scale(Dx,0.5);
        }while(gsl_blas_dnrm2(fz)>(1-s/2)*gsl_blas_dnrm2(fx) && s>0.02 );

        gsl_vector_memcpy(x,z);
        gsl_vector_memcpy(fx,fz);

    }while( gsl_blas_dnrm2(fx)>eps );


    *iter = calls;

    gsl_matrix_free(J);
    gsl_matrix_free(R);
    gsl_vector_free(fx);
    gsl_vector_free(fz);
    gsl_vector_free(z);
    gsl_vector_free(df);
    gsl_vector_free(Dx);
}















