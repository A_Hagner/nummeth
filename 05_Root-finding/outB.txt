Exercise B) 
 Newton's method with analytical Jacobian.

The Himmelblau function.
 The function has a root at (x,y)=     3.000          2.000     

The function has a second root at (x,y)=    -2.805          3.131     

The function has a third root at (x,y)=    -3.779         -3.283     

The function has a fourth root at (x,y)=     3.584         -1.848     


Comparison for the Himmelblau for Analytical Jacobian and numeric Jacobian (x0,y0)=(100,-10)
Numeric Jacobian
Uses #calls = 13
Analytical Jacobian
Uses #calls = 1
