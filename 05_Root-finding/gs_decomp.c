#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

/*

//Always A(n) and Q(m)!
void matrix_col_set(gsl_matrix*A, int n, gsl_matrix*Q, int m){
    for(int i=0; i<=n; i++){
        gsl_matrix_set(Q,i,m,gsl_matrix_get(A,i,n));
    }
}


//Q(|,m) and A(|,n) are the two columms which get dotproducted ; Q and A should be the same size
double matrix_col_prod(gsl_matrix*A,gsl_matrix*Q, int n, int m){

    for(int i=0;i<A->size1;i++){
        dotp+=gsl_matrix_get(A,i,n)*gsl_matrix_get(Q,i,m);
    }

return dotp;}
*/

void backsub(gsl_matrix*A, gsl_vector*c){
    for(int i=c->size-1; i>=0; i--){
        double ci=gsl_vector_get(c,i);
        for(int j=i+1; j<c->size; j++){
            ci -= gsl_matrix_get(A, i, j) * gsl_vector_get(c, j);
        }
        gsl_vector_set(c,i,ci/gsl_matrix_get(A,i,i));
    }
}



void qr_gs_decomp(gsl_matrix*A, gsl_matrix*R){
    double Rij;

    for(int i=0;i<A->size2;i++){
        gsl_vector_view ai=gsl_matrix_column(A,i); //Lets you work with vectors while they are in a matrix
        double Rii=(gsl_blas_dnrm2(&ai.vector)); //calculating diagonal elements
        gsl_matrix_set(R,i,i,Rii); //setting Rii diagonal
        gsl_vector_scale(&ai.vector,1.0/Rii);

        for(int j=i+1;j<A->size2;j++){
            gsl_vector_view aj=gsl_matrix_column(A,j);

            gsl_blas_ddot (&ai.vector,&aj.vector,&Rij); //DOTS
            gsl_matrix_set(R,i,j,Rij);
            gsl_blas_daxpy (-Rij,&ai.vector , &aj.vector); //This does a*x+y

            gsl_vector_scale(&ai.vector,1.0/gsl_blas_dnrm2(&ai.vector));
        }

    }

}


void qr_gs_solve(const gsl_matrix*Q, const gsl_matrix*R, gsl_vector*b, gsl_vector*x){

    gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); //Dles Q^T*b saves in b. (Found in BLAS level 2)

    backsub(R,x);
}

void qr_gs_inverse(const gsl_matrix*Q, const gsl_matrix*R, gsl_matrix*B){
    gsl_matrix_set_identity(B);
    gsl_vector*x = gsl_vector_alloc(Q->size1);

    for(int i=0;i<Q->size1;i++){
       gsl_vector_view b=gsl_matrix_column(B,i);
       qr_gs_solve(Q,R,&b.vector,x);
        gsl_matrix_set_col(B,i,x);
    }
}


int print_matrix(gsl_matrix * M){
    int n=(*M).size1, m=(*M).size2;
    for (int row=0; row<n; row++)
    {
        for(int columns=0; columns<m; columns++){
            printf("%10.3f     ", gsl_matrix_get(M,row,columns));}
        printf("\n");
    }
    return 0;
}

int print_vector(gsl_vector * M){
    int n=(*M).size;
    for (int row=0; row<n; row++){
        printf("%10.3f     ", gsl_vector_get(M,row));}
    printf("\n");
    return 0;
}


//gsl_matrix_set(R,i,i,sqrt(matrix_col_prod(A,A,i,i))); //Diagonalen i R





















