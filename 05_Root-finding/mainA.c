#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

int print_matrix(gsl_matrix * M);

int print_vector(gsl_vector * M);

void function_one(gsl_vector*t,gsl_vector*df);

void rosenbrock(gsl_vector*t,gsl_vector*df);

void himmelblau(gsl_vector*t,gsl_vector*df);

void NewtonA(void f(gsl_vector*x,gsl_vector*fx), gsl_vector*xstart, double dx, double eps, int*iter);





int main(){
    gsl_vector*x= gsl_vector_calloc(2);

    fprintf(stdout,"Exercise A) \n Newton's method with back-tracking linesearch and numerical Jacobian.\n\n");

    int o=0;
    //Fucntion One
    double xstart1 = 0.5;
    double xstart2 = 8.5;
    gsl_vector_set(x,0,xstart1);
    gsl_vector_set(x,1,xstart2);
    double dx = 0.0001;
    double eps = 10e-5;

    NewtonA(function_one,x,dx,eps,&o);
    fprintf(stdout,"System of Equations.\n The system has a solution at (x,y)=");
    print_vector(x);


    gsl_vector_set(x,0,xstart2);
    gsl_vector_set(x,1,xstart1);
    NewtonA(function_one,x,dx,eps,&o);
    fprintf(stdout," And another solution at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");


    //ROsenbrock
    double xs1 = 0.8;
    double xs2 = 0.8;
    gsl_vector_set(x,0,xs1);
    gsl_vector_set(x,1,xs2);
    dx = 0.0001;
    eps = 10e-5;

    NewtonA(rosenbrock,x,dx,eps,&o);
    fprintf(stdout,"The Rosenbrock function.\n The function has a root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");


    //Himmelblau
    double x01 = 2;
    double x02 = 1.5;
    gsl_vector_set(x,0,x01);
    gsl_vector_set(x,1,x02);
    dx = 0.0001;
    eps = 10e-5;
    NewtonA(himmelblau,x,dx,eps,&o);
    fprintf(stdout,"The Himmelblau function.\n The function has a root at (x,y)=");
    print_vector(x);

    gsl_vector_set(x,0,-2.8);
    gsl_vector_set(x,1,3.1);
    NewtonA(himmelblau,x,dx,eps,&o);
    fprintf(stdout,"The function has a second root at (x,y)=");
    print_vector(x);

    gsl_vector_set(x,0,-3.7);
    gsl_vector_set(x,1,-3.2);
    NewtonA(himmelblau,x,dx,eps,&o);
    fprintf(stdout,"The function has a third root at (x,y)=");
    print_vector(x);

    gsl_vector_set(x,0,3.5);
    gsl_vector_set(x,1,-1.8);
    NewtonA(himmelblau,x,dx,eps,&o);
    fprintf(stdout,"The function has a fourth root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");

    return 0;}






















