#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include <gsl/gsl_blas.h>

int print_matrix(gsl_matrix * M);

int print_vector(gsl_vector * M);

void himmelblau(gsl_vector*t,gsl_vector*df);

void jacobi_himmelblau( gsl_vector*t, gsl_matrix*j );

void NewtonA(void f(gsl_vector*x,gsl_vector*fx), gsl_vector*xstart, double dx, double eps, int*iter);

void NewtonB(void f(gsl_vector*x,gsl_vector*fx), gsl_vector*x, void jacobi(gsl_vector*x, gsl_matrix*J), double dx, double eps, int *iter);


int main(){
    gsl_vector*x= gsl_vector_calloc(2);

    fprintf(stdout,"Exercise B) \n Newton's method with analytical Jacobian.\n\n");
    int* o;
    //Himmelblau
    double x01 = 2;
    double x02 = 1.5;
    gsl_vector_set(x,0,x01);
    gsl_vector_set(x,1,x02);
    double dx = 0.0001;
    double eps = 10e-7;

    NewtonB(himmelblau, x, jacobi_himmelblau, dx, eps,o);
    fprintf(stdout,"The Himmelblau function.\n The function has a root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");


    gsl_vector_set(x,0,-2.8);
    gsl_vector_set(x,1,3.1);
    NewtonB(himmelblau, x, jacobi_himmelblau, dx, eps,o);
    fprintf(stdout,"The function has a second root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");


    gsl_vector_set(x,0,-3.2);
    gsl_vector_set(x,1,-3.7);
    NewtonB(himmelblau, x, jacobi_himmelblau, dx, eps,o);
    fprintf(stdout,"The function has a third root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n");

    gsl_vector_set(x,0,3.5);
    gsl_vector_set(x,1,-1.8);
    NewtonB(himmelblau, x, jacobi_himmelblau, dx, eps,o);
    fprintf(stdout,"The function has a fourth root at (x,y)=");
    print_vector(x);
    fprintf(stdout,"\n\n");






    //SAFMLKASMF
    gsl_vector_set(x,0,100);
    gsl_vector_set(x,1,-10);

    //FUCK POINTERS! C HAR DEN DÅRLIGSTE POINTER SYNTAX
    int A = 0;
    int B = 0;

    fprintf(stdout,"Comparison for the Himmelblau for Analytical Jacobian and numeric Jacobian (x0,y0)=(100,-10)\n");
    NewtonA(himmelblau, x, dx, eps, &A);
    fprintf(stdout,"Numeric Jacobian\n");
    fprintf(stdout,"Uses #calls = %i\n",A);

    NewtonB(himmelblau, x, jacobi_himmelblau, dx, eps, &B);
    fprintf(stdout,"Analytical Jacobian\n");
    fprintf(stdout,"Uses #calls = %i\n",B);

    return 0;}






















//
// Created by hagner on 6/20/17.
//

